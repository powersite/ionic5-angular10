// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyBPs6cBHmsukei3yFnCc-die19L_GOGNKE',
    authDomain: 'angular10-93b2d.firebaseapp.com',
    databaseURL: 'https://angular10-93b2d.firebaseio.com',
    projectId: 'angular10-93b2d',
    storageBucket: 'angular10-93b2d.appspot.com',
    messagingSenderId: '344361381041',
    appId: '1:344361381041:web:7ec85bc220137d95ec0690',
    measurementId: 'G-CXCFE8WP7M'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
