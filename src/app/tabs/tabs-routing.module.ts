import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'tabAdd',
        loadChildren: () => import('./tabAdd/tabAdd.module').then(m => m.Tab1PageModule)
      },
      {
        path: 'tabDash',
        loadChildren: () => import('./tabDash/tabDash.module').then(m => m.Tab2PageModule)
      },
      {
        path: 'tabList',
        loadChildren: () => import('./tabList/tabList.module').then(m => m.Tab3PageModule)
      },
      {
        path: '',
        redirectTo: '/tabs/tabDash',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/tabDash',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
