import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import {ModalController} from '@ionic/angular';
import {FinancesService} from '../../../core/services/finances.service';
import {ActivatedRoute} from '@angular/router';
import {AlertComponent} from '../../../components/alert/alert.component';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import { UpdaterecordComponent } from './updaterecord.component';

xdescribe('UpdaterecordComponent', () => {
  let component: UpdaterecordComponent;
  let fixture: ComponentFixture<UpdaterecordComponent>;
  let modalController: ModalController;
  let financesService: FinancesService;
  let activatedRoute: ActivatedRoute;
  let alertComponent: AlertComponent;
  let formBuilder: FormBuilder;
  let formGroup: FormGroup;
  let validators: Validators;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdaterecordComponent ],
      imports: [IonicModule.forRoot(),
                ModalController,
                FinancesService,
                ActivatedRoute,
                AlertComponent,
                FormBuilder, FormGroup, Validators]
    }).compileComponents();

    modalController =  TestBed.inject(ModalController);
    financesService =  TestBed.inject(FinancesService);
    activatedRoute =  TestBed.inject(ActivatedRoute);
    alertComponent =  TestBed.inject(AlertComponent);
    formBuilder =  TestBed.inject(FormBuilder);
    formGroup =  TestBed.inject(FormGroup);
    validators =  TestBed.inject(Validators);

    fixture = TestBed.createComponent(UpdaterecordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
