import { Component, OnInit, Input } from '@angular/core';

import {ModalController} from '@ionic/angular';
import {FinancesService} from '../../../core/services/finances.service';
import {ActivatedRoute} from '@angular/router';
import {AlertComponent} from '../../../components/alert/alert.component';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-updaterecord',
  templateUrl: './updaterecord.component.html',
  styleUrls: ['./updaterecord.component.scss'],
})
export class UpdaterecordComponent implements OnInit {
  addrecord: any = {};
  ionicForm: FormGroup;
  isSubmitted = false;

  constructor(
      public modalController: ModalController,
      public financesService: FinancesService,
      public alertComponent: AlertComponent,
      public activatedRoute: ActivatedRoute,
      public formBuilder: FormBuilder
  ) {
    this.addrecord = this.activatedRoute.snapshot.paramMap.get('record');
  }

  ngOnInit() {
    this.ionicForm = this.formBuilder.group({
      amount: ['', [Validators.required, Validators.pattern('^[0-9]+$')]],
      description: ['', [Validators.required, Validators.minLength(3)]],
      type: ['', [Validators.required, Validators.minLength(3)]]
    });
  }

  submitForm() {
    this.isSubmitted = true;
    if (!this.ionicForm.valid) {
      return false;
    } else {
      this.financesService.editRecord(this.ionicForm.getRawValue(), this.addrecord).then((success) => {
        // tslint:disable-next-line:no-shadowed-variable
        this.alertComponent.presentAlert('Registro almacenado!').then((success) => {
          this.ionicForm.reset();
          this.CloseModal();
        });
      });
    }
  }

  CloseModal(){
    this.modalController.dismiss();
  }

  get errorControl() {
    return this.ionicForm.controls;
  }


}
