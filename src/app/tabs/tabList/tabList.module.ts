import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TabListPage } from './tabList.page';
import { Tab3PageRoutingModule } from './tabList-routing.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    RouterModule.forChild([{ path: '', component: TabListPage }]),
    Tab3PageRoutingModule,
  ],
  declarations: [TabListPage]
})
export class Tab3PageModule {}
