import { Component } from '@angular/core';
import {FinancesService} from '../../core/services/finances.service';
import {ModalController} from '@ionic/angular';
import {AlertComponent} from '../../components/alert/alert.component';
import {UpdaterecordComponent} from './updaterecord/updaterecord.component';
import {Router} from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tabList.page.html',
  styleUrls: ['tabList.page.scss']
})
export class TabListPage {

  records: any = [];
  constructor(private financesService: FinancesService,
              private modalController: ModalController,
              public alertComponent: AlertComponent,
              public router: Router) {}

  // tslint:disable-next-line:use-lifecycle-interface
  ngOnInit(){
    this.financesService.getRecords().valueChanges()
        .subscribe((recordFB) => {
          this.records = recordFB;
        }, error => {
          this.alertComponent.presentAlert('El sistema no se encuentra disponible, intente nuevamente más tarde');
        });

  }

  async UpdateRecord(record) {
    const modal = await this.modalController.create({
      component:  UpdaterecordComponent,
      cssClass: 'my-custom-class',
      componentProps: {
        addrecord: record
      }
    });
    return await modal.present();
  }

  DeleteRecord(record){
    this.alertComponent.alertConfirm(record.description).then((success) => {
      this.financesService.deleteRecord(record);
    });
  }

  NewRecord(){
    this.router.navigate(['/tabs/tabAdd']);
  }

  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = `Error: ${error.error.message}`;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }

}
