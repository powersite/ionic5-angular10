import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TabDashPage } from './tabDash.page';

xdescribe('Tab2Page', () => {
  let component: TabDashPage;
  let fixture: ComponentFixture<TabDashPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TabDashPage],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TabDashPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
