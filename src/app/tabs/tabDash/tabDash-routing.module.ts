import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabDashPage } from './tabDash.page';

const routes: Routes = [
  {
    path: '',
    component: TabDashPage,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Tab2PageRoutingModule {}
