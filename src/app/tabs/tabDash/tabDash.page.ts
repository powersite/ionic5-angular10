import { Component } from '@angular/core';
import {FinancesService} from '../../core/services/finances.service';
import {ModalController, ToastController} from '@ionic/angular';
import {CoreModule} from '../../core/core.module';
import {AlertComponent} from '../../components/alert/alert.component';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tabDash.page.html',
  styleUrls: ['tabDash.page.scss']
})
export class TabDashPage {

  income = 0;
  expense = 0;
  constructor(private financesService: FinancesService,
              public codeModule: CoreModule,
              public alertComponent: AlertComponent) {

      this.financesService.getRecordsIncome().valueChanges().subscribe((recordFB: any[]) => {
          this.income = recordFB.reduce((accum, item) => accum + item.amount, 0 );
          this.codeModule.presentToast();
      }, error => {
          this.alertComponent.presentAlert(error);
      });

      this.financesService.getRecordsExpense().valueChanges().subscribe((recordFB: any[]) => {
          this.expense = recordFB.reduce((accum, item) => accum + item.amount, 0 );
          this.codeModule.presentToast();
      }, error => {
          this.alertComponent.presentAlert(error);
      });
  }
}
