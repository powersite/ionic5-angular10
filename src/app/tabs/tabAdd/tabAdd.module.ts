import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { TabAddPage } from './tabAdd.page';
import { Tab1PageRoutingModule } from './tabAdd-routing.module';
import {  UpdaterecordComponent } from '../tabList/updaterecord/updaterecord.component';

@NgModule({
    imports: [
        IonicModule,
        CommonModule,
        FormsModule,
        Tab1PageRoutingModule,
        ReactiveFormsModule
    ],
  entryComponents: [UpdaterecordComponent],
  declarations: [TabAddPage, UpdaterecordComponent]
})
export class Tab1PageModule {}
