import { Component, OnInit } from '@angular/core';
import {FinancesService} from '../../core/services/finances.service';
import {AlertComponent} from '../../components/alert/alert.component';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import {Router} from '@angular/router';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tabAdd.page.html',
  styleUrls: ['tabAdd.page.scss']
})
export class TabAddPage implements OnInit {

  ionicForm: FormGroup;
  isSubmitted = false;

  constructor(
      private financesService: FinancesService,
      public alertComponent: AlertComponent,
      public formBuilder: FormBuilder,
      public router: Router
  ) {}

  ngOnInit(){
    this.ionicForm = this.formBuilder.group({
      amount: ['', [Validators.required, Validators.pattern('^[0-9]+$')]],
      description: ['', [Validators.required, Validators.minLength(3)]],
      type: ['', [Validators.required, Validators.minLength(3)]]
    });
  }

  submitForm() {
    this.isSubmitted = true;
    if (!this.ionicForm.valid) {
      return false;
    } else {
      console.log(this.ionicForm.getRawValue());
      this.financesService.createRecord(this.ionicForm.getRawValue()).then((success) => {
        // tslint:disable-next-line:no-shadowed-variable
        this.alertComponent.presentAlert('Registro almacenado!').then((success) => {
          this.ionicForm.reset();
          this.router.navigate(['/tabs/tabList']);
        });
      });
    }
  }

  get errorControl() {
    return this.ionicForm.controls;
  }

}
