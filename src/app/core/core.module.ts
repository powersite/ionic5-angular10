import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ToastController} from '@ionic/angular';



@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class CoreModule {

  constructor(public toastController: ToastController) {
  }

  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Se actualizaron datos!',
      duration: 2000
    });
    toast.present();
  }
}
