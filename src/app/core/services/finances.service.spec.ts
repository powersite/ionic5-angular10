import { TestBed } from '@angular/core/testing';
import { Injectable } from '@angular/core';

import { FinancesService } from './finances.service';
import {ManagerService} from './manager/manager.service';

xdescribe('FinanzasService', () => {
  let service: FinancesService;
  let manager: ManagerService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ManagerService, Injectable, FinancesService]
    });
    service = TestBed.inject(FinancesService);
    manager = TestBed.inject(ManagerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
