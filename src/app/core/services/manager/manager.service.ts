import { Injectable } from '@angular/core';
import {AngularFireDatabase} from '@angular/fire/database';
import {HttpErrorResponse} from '@angular/common/http';
import {throwError} from 'rxjs';
import * as Sentry from '@sentry/angular';

@Injectable({
  providedIn: 'root'
})
export class ManagerService {

  constructor(public afDB: AngularFireDatabase) { }

  public getRecords(space){
    return this.afDB.list(space);
  }

  public getRecordsByAttribute(space, path, value){
    return this.afDB.list(space, ref => ref.orderByChild(path).equalTo( value));
  }

  public getRecord(space, id){
    return this.afDB.object(space + id);
  }

  public createRecord(space, record){
    record.id = Date.now();
    return this.afDB.database.ref(space + record.id).set(record).catch(error => {
      this.handdleError(error);
    });
  }

  public editRecord(space, editRecord, record){
    editRecord.id = record.id;
    return this.afDB.database.ref(space + editRecord.id).set(editRecord).catch(error => {
      this.handdleError(error);
    });
  }

  public deleteRecord(space, record){
    return this.afDB.database.ref(space + record.id).remove().catch(error => {
      this.handdleError(error);
    });
  }

  private handdleError(error: HttpErrorResponse){
    console.log(error);
    Sentry.captureException(error);
    return throwError('Ups algo no funciona, estamos trabajando en ello.');
  }
}
