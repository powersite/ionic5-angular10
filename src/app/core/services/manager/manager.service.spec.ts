import { TestBed } from '@angular/core/testing';

import { ManagerService } from './manager.service';
import {AngularFireDatabase} from '@angular/fire/database';
import {HttpErrorResponse} from '@angular/common/http';

import {throwError} from 'rxjs';
import * as Sentry from '@sentry/angular';

xdescribe('ManagerService', () => {
  let service: ManagerService;
  // tslint:disable-next-line:prefer-const
  let afDB: AngularFireDatabase;
  // tslint:disable-next-line:prefer-const
  let httpErrorResponse: HttpErrorResponse;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [AngularFireDatabase, HttpErrorResponse]
    });
    service = TestBed.inject(ManagerService);
    afDB = TestBed.inject(AngularFireDatabase);
    httpErrorResponse = TestBed.inject(HttpErrorResponse);
    // tslint:disable-next-line:label-position no-unused-expression
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
