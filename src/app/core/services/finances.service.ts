import { Injectable } from '@angular/core';
import {ManagerService} from './manager/manager.service';

@Injectable({
  providedIn: 'root'
})
export class FinancesService {

  private placeDB = '/records/';
  constructor(private managerServiceDB: ManagerService) {
  }

  public getRecords(){
    return this.managerServiceDB.getRecords(this.placeDB);
  }

  public getRecordsIncome(){
    return this.managerServiceDB.getRecordsByAttribute(this.placeDB, 'type', 'Ingreso');
  }

  public getRecordsExpense(){
    return this.managerServiceDB.getRecordsByAttribute(this.placeDB, 'type', 'Egreso');
  }

  public getRecord(id){
    return this.managerServiceDB.getRecord(this.placeDB, id);
  }

  public createRecord(record){
    return this.managerServiceDB.createRecord(this.placeDB, record);
  }

  public editRecord(editRecord, record){
    return this.managerServiceDB.editRecord(this.placeDB, editRecord, record);
  }

  public deleteRecord(record){
    return this.managerServiceDB.deleteRecord(this.placeDB, record);
  }

}
